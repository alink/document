# startActivityForResult() deprecated 에 따른 대체 Activity Result API 알아보기

## 호출한 액티비티로 부터 결과 받아오는 방법

### MainActivity에서 > SubActivity를 여는 경우 ###

* Activity MainActivity 에서 SubActivity 를 호출

* Activity SubActivity 에서 작업 처리.

* 처리된 작업 결과 값을 Activity MainActivity 로 반환.

### 기존방법 startActivityForResult

* MainActivity 소스코드

```
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Intent calIntent = new Intent(this, SubActivity.class);
		startActivityForResult(calIntent, 3000);
		
    }

    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch (requestCode){
                // MainActivity 에서 요청할 때 보낸 요청 코드 (3000)
                case 3000:
                    // TODO
                    break;
            }
        }
    }
}
```

* SubActivity 소스코드

```
		Intent resultIntent = new Intent();
        resultIntent.putExtra("result","");
        setResult(RESULT_OK,resultIntent);
        finish();

```

## 신규 Activity Result API

### 구글이 startActivityForResult deprecated 한 이유는 뭘까?
1. ◼ Tight Coupling (강한 결합)
 - onActivityResult 로 결과가 오기때문에 여러화면 호출시 분리가 어려운 구조
2. 메모리유실이 될수 있다고 하는걸로 보아 안정성 때문인걸로 추측

#### AndroidX란? 

- Android 9.0(API 수준 28)의 출시와 함께 Jetpack의 일부인 새로운 버전의 지원 라이브러리 AndroidX가 출시됐다.
- 기존 support라이브러리도 사용할 수 있으나 새로운 개발은 AndroidX에서 진행되기 때문에 변환하는 것이 좋다.
- AndroidX는 기존에 사용 중인 com.android.support.* 라이브러리들을 하나로 통합한 것이라고 생각하시면 됩니다.
- AndroidX는 안드로이드 스튜디오 버전 3.2 이상 빌드툴 28이상에서 사용가능

#### Android jetpack이란?

- Android jetpack은 멋진 Android앱을 만들기 위한 컴포넌트, 도구 및 지침 세트이다.
- hilt, databinding, viewbinding, compose, room, viewmodel 등등

### Activity Result API를 사용하면 좋은 이유? 
1. ◼ boilerplate code 가 줄어듦
 - OnActivityResult 를 override 할 필요가 없고, requestCode 를 확인하는 일도 없다. 등록한 콜백에서 결과를 처리하기만 하면 된다.
2. ◼ 가독성 증가
 - 하나의 launcer 는 하나의 contract 를 실행하고 그 결과를 하나의 callback 으로 반환한다.


* 사전에 Dependency 추가
```
dependencies {
  implementation 'androidx.appcompat:appcompat:1.3.1'
}
```

* MainActivity 소스코드

```
public class MainActivity extends AppCompatActivity {


    // Pre contract
    private ActivityResultLauncher<Intent> mPreContractStartActivityResult =
            registerForActivityResult(
                    new ActivityResultContracts.StartActivityForResult(),
                    new ActivityResultCallback<ActivityResult>() {
                        @Override
                        public void onActivityResult(ActivityResult a_result) {
                            if (a_result.getResultCode() == Activity.RESULT_OK) {
                                
                            }
                        }
                    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Intent intent = new Intent(getApplication(), SubActivity.class);
        mPreContractStartActivityResult.launch(intent);
		
    }
}
```

* SubActivity 소스코드

```
		Intent resultIntent = new Intent();
        resultIntent.putExtra("result","");
        setResult(RESULT_OK,resultIntent);
        finish();

```
